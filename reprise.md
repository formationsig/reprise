# ![logo](images/passif.png)  Reprise de données anciennes



## 1. [Adobe Illustrator] Nettoyage et préparation de la figure

- pas de sous-calque ; remarque : entités contenues dans un sous-groupe issu de autocad parfois récupérées quand même (SPLINE, LWPOLYLINE,..)
- pas de groupe ;
- pas de pot de peinture dynamique ;
- fermeture de toutes les formes ;
- ajout des vestiges éventuellement non dessinés ;
- découpage des formes continues qui composent plus de 2 faits avec le pathfinder division ;
- dégroupage des formes générées ;
- regrouper les entités de même nature dans un même calque (faits, tranchées,...). Sauf si par exemple, calque par période ou/et par type de fait existe ;
- ajout de points d'ancrage et simplification des tracés (cocher lignes droites).

|![simplifier](images/1_simplifier.png)|
|:--:|
|*Illustrator - simplification : objet -> tracé ->simplifier*|

- suppression des courbes de Béziers ;suppression des points isolés ;
- *Option : positionner le point d'ancrage du texte relatif au numéro de vestige à l'intérieur de chaque forme pour préparer la jointure spatiale ; peut être effectué dans QGIS ultérieurement aussi.*

|![ancrage](images/2_ancrage.png)|
|:--:|
|*Illustrator: ajout de points d'ancrage*|

- noms de calques sans caractère spécial de type ```/```



## 2. Export en dxf

- Exporter le format ai nettoyé en dxf. Version 2000 ou ultérieure ;

  fichier → exporter. Choisir Fichier AutoCAD Interchange (*.DXF) comme Type, puis

|![export_dxf](images/3_dxf.png)|
|:--:|
|*Illustrator - Export DXF*|



- Le reste des paramètres est facultatif.

*Option : si une échelle est présente sur le plan ai et qu'elle est juste, on peut utiliser l'option « mise à l'échelle de l'illustration » sachant que Échelle = 1 cm correspondra au dénominateur de l'échelle en m (unités...du SIG). Ici 1 cm = 25 m. Cela mettra à l'échelle le dxf et le géoréférencement reste à faire.*



## 3. [QGIS] Création des shp 

-  Sous QGIS, chargement du dxf comme couche vecteur ![icon_ajouter_vecteur](images/icon_ajouter.png)
- sélection du SCR 2154 ;
- sélection des deux ou trois couches de géométrie différente en fonction

|![selection_vecteur](images/4_selection_shp.png)|
|:--:|
|*QGIS - Selection des couches à ajouter*|

* Export des couches issues du dxf en shapefile suivant le modèle conceptuel **des 6 couches** SIG d'une opération archéologique :
  - Dxxxxxx_emprise
  - Dxxxxxx_ouverture
  - Dxxxxxx_poly

Parfois, certaines formes fermées sous AI ne sont pas récupérées en polygone mais juste en polylignes fermées. Conversion en polygone nécessaire.

>  *Tips : un copier/coller d'une ligne fermée sur une couche de geometrie polygone effectue la conversion.*

S’il manque de nombreux polygones, on gagnera du temps à partir de la couche de polylignes et la convertir entièrement en polygones.

* Dxxxx_points_annotation

	* *etc*



## 4. Nettoyage des shapefiles : la couche poly

- Vérification si présence de geometrie null  

*ex : virtual_layer :*

```sql
select * from Dxxxx_poly where geometry is null
```

- Vérification si erreur de géométrie

On peut utiliser les outils de base de qgis du menu vecteur → outils de géométrie → vérifier la validité des géométries. ![validité](images/5_validite.png)

Ou encore utiliser une fonction dédiée dans un virtual layer dont la table attributaire recense les erreurs :

```sql
select numpoly, st_isvalidreason(geometry), geometry as geom from Dxxxx_poly where st_isvalid(geometry) = 0
```

- *Tips* pour éliminer des doublons de nœuds :

```sql
select numpoly, typoly,… st_buffer(geometry, 0) as geom FROM Dxxxxxx_poly 
```

A utiliser en complément avec l'éditeur de nœuds si aucune solution n'est parfaite. 

- création des multipolygones éventuellement

*exemple : un fossé suivi dans plusieurs tranchées.*




## 5. Jointure spatiale pour récupérer les identifiants

Les identifiants sont contenus dans la table attributaire des points. Ces points se trouvent à une certaine distance des polygones auxquels ils se rattachent. Deux possibilités : 

- (possiblement réalisable sous AI :) déplacer les points à l'intérieur des polygones correspondant puis réaliser une jointure spatiale classique avec les outils basiques de QGis, soit vecteur → outil de gestion de données → joindre les attributs par localisation.
- Buffers et jointure spatiale :

L’utilisation de buffer (tampons) sur la couche de points peut se substituer au déplacement des points dans les polygones vu ci-dessus. L’idée est de s ‘assurer qu’un maximum de buffers « intersect » les polygones afin que la jointure par localisation produise le résultat attendu. Efficace pour des vestiges peu concentrés.

- Création de la couche de buffers  ![buffer](images/6_buffer.png)

> *Note: Faire des essais de buffer pour trouver le rayon adéquat soit celui qui permettra à chaque buffer de « couper » un polygone.*

|![buffer](images/7_buffer2.png)|![buffer](images/8_buffer3.png)|
|:--:|:--:|
|*Interface de l'outil Tampon*|*résultat de l'outil Tampon*|

Les buffers (cercles) coupent au moins un polygone. On remarque toutefois que chaque polygone n’est pas coupé par un buffer, ce qui laisse supposer fortement que certains polygones n’auront pas d’attributs à ce stade.

*  Jointure spatiale: Entre la couche de buffers créée, appelée par défaut **Mis en tampon** et **Dxxxxxx_poly**.

|![joindre](images/9_joindre.png)|
|:--:|
|*QGIS -Joindre les attributs par localisation* |

La couche créée sera exportée en Dxxxxxx_poly2.



> *BONUS :*
>
> * *Utiliser les virtual layers pour réaliser une jointure spatiale en demandant de joindre à chaque polygone l'attribut du point le plus proche, qui contient le numéro de fait. Efficace pour des vestiges peu concentrés.*
> * *Jointure avec création d'un buffer autour du point pour intersecter les polygones. Faire des essais de buffer pour trouver le rayon du buffer adéquat.*	
>
> ```sql
> select p.\*, a.Text from Dxxxx**xx**_poly as p join 
> Dxxxx**xx**_points_annotation a on st_intersects(p.geometry,
>  st_buffer(a.geometry, 0.07)) where a.Layer like '001%'
> ```
>
> |![visualisation_buffers](images/10_visu_buffer.png)|
> |:--:|
> |*QGIS - visualisation des buffers autour des points*|
> **ou**
> * *Jointure en indiquant la distance du point au polygone. :warning: Faire des essais pour trouver la bonne distance.*
>
> ```sql
> select p.*, a.Text from Dxxxx_poly as p
	join Dxxxxxx_points_annotation as a
  	on ptDistWithin(p.geometry, a.geometry, 0.07)
  where a.Layer like '001%'
> ```

* Export du virtual layer en Dxxxxxx_poly2

* Vérification des données : 

  * doublons de numéros
  * vestiges sans numéro

  

## 6. Mise à jour champ numpoly

* Structuration de la table attributaire en accord avec la couche poly des 6 couches opération.

> *exemple de traitement (variant en fonction du jeu de données) :*
>
> ![maj_champs](images/11_maj_champs.png)
>
> * Les derniers chiffres du champ Text_1 (issu de la jointure) sont le numéro de fait, à copier dans numpoly.
>
> * **Calculatrice de champs** et **mise à jour du champ** "numpoly ":  ici, extraire les chiffres en partant de la fin du champ
>
>   *  ``` right(Text_1, 2)``` puis ```replace(numpoly, ';', '')```
>
>   **ou**
>   
>  * ``` regexp_substr("Text_1", '(\d+)$')```




* export du shp en Dxxxxxx_poly3



## 7. Jointure attributaire avec inventaire faits

Opération classique avec un inventaire des vestiges au format MS Excel (.xls) ou LibreO (.odt) et mise à jour du champ "interpret", voire "datedebut "et "datefin "si l'information est disponible.



## 8. Ajustement spatial avec l'extension QGIS Vector Bender 

En fonction des données du document initial l'ajustement spatial peut être réalisé en :

* établissant la correspondance entre le parcellaire issu du plan *Adobe Illustrator (c)* et le parcellaire géoréferencé type WFS Bdparcellaire ;

* utilisant des points propres à l'opération, dans le cas où l'on a l'emprise prescrite géoréferencée (ou le pdf de la prescription géoréferençable), comme dans l'exemple ci-dessous :

  * Installation de l'extension

|![vector_bender_instalation](images/12_vector_bender.png)|
|:--:|
|*QGIS - Installation de l'extension Vector Bender*|

* 
  * description et utilisation

|![vector_bender_instalation](images/13_vector_bender2.png)|
|:--:|
|*QGIS - Interface de l'extension Vector Bender*|

**Paramètres principaux :**

*Layer to bend* : couche à géoréferencer ;

*Pairs layer* : l’icône vecteur ![ajouter](images/icon_nouveau.png) permet de créer une couche temporaire **Vector Bender ** contenant les points utilisés pour le géoréferencement. :warning: **ATTENTION** SCR WGS84 par défaut ! Changer pour la projection dans laquelle on travaille. 

|![couches](images/14_panneau_couche.png)|
|:--:|
|*QGIS - panneau  Couches: création de la couche temporaire **Vector Bender***|

En **mode édition sur cette couche *Vector Bender***, on crée deux paires de points en utilisant l’emprise présente dans la couche de lignes issue du dxf et la couche ***D002076_emprise* géoréferencée**. 

> *Note: Les options d’accrochage activées sur les deux couches concernées – Vector Bender et Dxxxxxx_poly2 faciliteront la tâche.*

| ![vb_transfo1](images/15_vb_transfo1.png) |      ![vb_transfo1](images/16_vb_transfo2.png)      |
| :---------------------------------------: | :-------------------------------------------------: |
|       *points d'origine sur le dxf*       | *points de destination sur l'emprise géoréférencée* |

* L’extension déduit la transformation adéquate en fonction du nombre de points ; ici méthode **Linear**. 

|![vb_transfo1](images/17_vector_bender3.png)|
|:--:|
|*QGIS - Interface de l'extension Vector Bender*|


* On vérifie que Dxxxxxx_poly3 est bien sélectionné ET en mode édition puis on clic sur [Run]

|![vb_transfo1](images/18_vb_transfo3.png)|
|:--:|
| *QGIS -Résultat de l'ajustement spatial avec Vector Bender* |



## 9. Exercice : Traiter de la même manière les ouvertures

* Récupérer les identifiants (couche points et ouvertures, jointure spatiale)
* Structurer la table attributaire (en respectant la norme des 6 couches)
* Géoréferencer les ouvertures
* Nettoyer la table attributaire.
```

```