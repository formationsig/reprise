# Summary

## Atelier - Reprise de données anciennes

* [1. Adobe Illustrator - Nettoyage et préparation de la figure](/reprise.md#1-adobe-illustrator-nettoyage-et-préparation-de-la-figure)
* [2. Export en dxf](/reprise.md#2-export-en-dxf)
* [3. QGIS -  Création des shp ](/reprise.md#3-qgis-création-des-shp)
* [4. Nettoyage des shapefiles : la couche poly](/reprise.md#4-nettoyage-des-shapefiles--la-couche-poly)
* [5. Jointure spatiale pour récupérer les identifiants](/reprise.md#5-jointure-spatiale-pour-récupérer-les-identifiants)
* [6. Mise à jour champ numpoly](/reprise.md#6-mise-à-jour-champ-numpoly)
* [7. Jointure attributaire avec inventaire faits](/reprise.md#7-jointure-attributaire-avec-inventaire-faits)
* [8. Ajustement spatial avec l'extension QGIS Vector Bender](/reprise.md#8-ajustement-spatial-avec-lextension-qgis-vector-bender)
* [9. Exercice: Traiter de la même manière les ouvertures](/reprise.md#9-exercice--traiter-de-la-même-manière-les-ouvertures)
