# ![logo](images/passif.png) Reprise de données anciennes

- durée: 1 journée
- pré-requis : niveau 1 découverte
- objectifs:
  - Du format Illustrator aux 6 couches opération (shapefile)

Supports de formation

- Ce support en ligne: https://formationsig.gitlab.io/reprise
- La version PDF à jour [![icon_pdf](images/pdf.png)](https://gitlab.com/formationsig/reprise/-/jobs/artifacts/master/raw/public/reprise.pdf?job=pages)
- Il existe aussi un ensemble de [![tutoriels video](images/video24.png) tutoriel vidéo)](https://intranet.inrap.fr/recuperation-du-passif-produire-des-shp-partir-dun-plan-illustrator-ou-dun-plan-scanne-qgis-310)

Données

* Les données sont en [accès restreint](\\promethese\partages-siege\Partages_Nationaux\Formations SIG\) pour les formateurs.

Ressources et documentation

* les scripts de préparation des 6 couches pour QGIS 3.xx
* Des fiches techniques peuvent  être utiles, vous les trouverez à l'adresse  https://formationsig.gitlab.io/fiches-technique



[ :leftwards_arrow_with_hook: Accueil - Formation SIG](https://formationsig.gitlab.io/toc/)



Toute cette documentation est sous licence sous licence [![CC](images/CC_BY_ND.png)](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)